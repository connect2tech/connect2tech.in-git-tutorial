connect2tech.in-Git-Tutorial(Master)
============================

# The largest heading

## The second largest heading

###### The smallest heading

**This is bold text**

*This text is italicized*

~~This was mistaken text~~

In the words of Abraham Lincoln:

> Pardon my French

Use `git status` to list all new or modified files that haven't yet been committed.

Some basic Git commands are:
```
git status
git add
git commit
```

This site was built using [GitHub Pages](https://pages.github.com/)

1. James Madison
2. James Monroe
3. John Quincy Adams

- [x] Finish my changes
- [ ] Push my commits to GitHub
- [ ] Open a pull request

 - Item 1
 - Item 2
  - Sub Item 1
  - Sub Item 2
  
https://help.github.com/en/github/collaborating-with-issues-and-pull-requests/resolving-a-merge-conflict-using-the-command-line
  
**Overwrite local changes**

- git fetch --all
- git reset --hard origin/master  

### Git Commands
- git init
- git status
- git add .
- git add -A [add tracked and untracked files. refer documentation]
- git commit -m "commit"
- git commit -am "commit" #directly committing (without adding)
- git push origin master
- git pull origin master
- git ls-files (list of files that are tracked in current repository)
- git log --oneline
- git log --oneline --decorate
- git log --oneline --graph --decorate

**revert changes: staging and locally**

- (use "git reset HEAD <file>..." to unstage) i.e. The file is in staging area.
- (use "git checkout -- <file>..." to discard changes in working directory)

**Renaming and Moving Files**

- git mv second.txt third.txt, git commit -m "file rename" [rename second.txt => third.txt]
- mv third.txt fourth.txt [os level copy, deleted:third.txt, Untracked files:fourth.txt, mv fourth.txt third.txt for revert]
- git mv second.txt third.txt; git mv third.txt second.txt [revert] 

**Deleting**

- git rm Sample.txt [on doing ls no there is no file in ws, git status will show "deleted:Sample.txt", perform git commit to make it permanent] 
- git reset HEAD Sample.txt [this will restore the file in git, but not in ws]
- git checkout Sample.txt [restore the file in ws; Even if the file is deleted, it will be restored]

**Comparison**

- git diff (local V/s staging)
- git diff HEAD (local V/s Repo)
- git diff --staged HEAD (stage V/s Repo)
- git diff -- one.txt
- git diff <id1> <id2> (All the changes from commit1 to commit2, /dev/null-It means files does not exist)
- git diff HEAD HEAD^
- git diff master origin/master (Repo V/s Remote Repo)

**Branching**

- git branch -a (show all branches; * on currently active branch)
- git branch branch1 (create a new branch)
- git checkout branch1 (switch to branch1, switching is only possible if current branch has nothing pending to commit)
- git branch -m branch1 branch2 (renaming)
- git branch -d branch1 (cannot delete a branch you are currently on)

- Branching and push to origin (Command details [Branching](https://www.atlassian.com/git/tutorials/learn-branching-with-bitbucket-cloud))

	- git branch test-1
	- git checkout test-1
	- git add editme.html
	- git commit editme.html -m'added a new quote'
	- git push origin test-1
	- git clone <url> --branch <branch> --single-branch
	(git clone https://bitbucket.org/connect2tech/samlink-webapplications --branch samlink-webapplications-branch --single-branch)
	(git clone https://bitbucket.org/connect2tech/samlink-leumi-webapplications --branch samlink-webapplications-branch --single-branch)
	- git push -u origin samlink-webapplications-branch (pushing it to remote branch)

**Merging**

![How to Merge](Merge.png)


- git checkout -b branch1 (create and switch to branch1; make change in file in current branch1 and commit;)
- git checkout master
- git diff master branch1
- git merge branch1 (Fast Forward. This is only possible if target branch, in this case master, does not have any change)
- git merge copy_right --no-ff ([FF vs no FF](https://stackoverflow.com/questions/6701292/git-fast-forward-vs-no-fast-forward-merge))


### Important Files
- ~/.gitconfig

name = connect2tech
email = message4naresh@gmail.com